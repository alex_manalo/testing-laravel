<?php

use App\Post;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class LikeTest extends TestCase
{
	use DatabaseTransactions;

	/** @test */
	public function a_user_can_like_a_post()
	{
		//given I have a post
		$post = factory(Post::class)->create();

		//and a user
		$user = factory(User::class)->create();

		//and that user is logged in
		$this->actingAs($user);

		//when they like a post
		$post->like();

		//then we should see evidence in the database and the post should be liked
		$this->seeInDatabase('likes', [
				'user_id'       => $user->id,
				'likeable_id'   => $post->id,
				'likeable_type' => get_class($post)
			]);

		$this->assertTrue($post->isLiked());
	}

	/** @test */
	public function a_user_can_unlike_a_post()
	{		
		$post = factory(Post::class)->create();

		$user = factory(User::class)->create();
		
		$this->actingAs($user);

		$post->like();
		$post->unLike();

		//then we should see evidence in the database and the post should be unliked
		$this->notSeeInDatabase('likes', [
				'user_id'       => $user->id,
				'likeable_id'   => $post->id,
				'likeable_type' => get_class($post)
			]);

		$this->assertFalse($post->isLiked());
	}

	/** @test */
	public function a_user_can_toggle_the_status_of_post()
	{
		$post = factory(Post::class)->create();

		$user = factory(User::class)->create();
		
		$this->actingAs($user);

		$post->toggle();		

		$this->assertTrue($post->isLiked());

		$post->toggle();		

		$this->assertFalse($post->isLiked());
	}

	/** @test */
	public function a_post_knows_how_many_likes_it_has()
	{
		$post = factory(Post::class)->create();

		$users = factory(User::class, 5)->create();
		
		foreach($users as $user)
		{
			$this->actingAs($user);		
			$post->like();
		}
		
		$this->assertEquals(5, $post->likesCount);
	}

}
