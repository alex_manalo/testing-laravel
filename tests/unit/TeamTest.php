<?php

use App\Team;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class TeamTest extends TestCase
{
	use DatabaseTransactions;

    /** @test */
    public function it_has_a_name()
    {
    	$team = new Team(['name' => 'Acme']);

    	$this->assertEquals('Acme', $team->name);
    }

    /** @test */
    public function it_can_add_members()
    {
    	$team = factory(Team::class)->create();

    	$user1 = factory(User::class)->create();
    	$user2 = factory(User::class)->create();

    	$team->add($user1);
    	$team->add($user2);

    	$this->assertEquals(2, $team->count());
    }

    /** @test */
    public function it_has_a_maximum_size()
    {
    	$team = factory(Team::class)->create(['size' => 2]);

    	$user1 = factory(User::class)->create();
    	$user2 = factory(User::class)->create();

    	$team->add($user1);
    	$team->add($user2);

    	$this->assertEquals(2, $team->count());

    	$this->setExpectedException('Exception');
    	$user3 = factory(User::class)->create();
    	$team->add($user3);
    }

    /** @test */
    // public function it_wont_allow_adding_multiple_members_at_once_if_it_exceeds_teams_max_size()
    // {
    //     $team = factory(Team::class)->create(['size' => 5]);

    //     $users = factory(User::class, 7)->create();        

    //     $this->setExpectedException('Exception');
        
    //     $team->add($users);
    // }

    /** @test */
    public function it_can_add_multiple_members_at_once()
    {
    	$team = factory(Team::class)->create();

    	$users = factory(User::class, 5)->create();

    	$team->add($users);

    	$this->assertEquals(5, $team->count());
    }

    /** @test */
    public function it_can_remove_a_member()
    {
        $team = factory(Team::class)->create();

        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $team->add($user1);
        $team->add($user2);

        $this->assertEquals(2, $team->count());

        $team->remove($user1);

        $this->assertEquals(1, $team->count());
    }

    /** @test */
    public function it_can_remove_multiple_members_at_once()
    {
        $team = factory(Team::class)->create(['size' => 5]);

        $users = factory(User::class, 5)->create();

        $team->add($users);

        $team->remove($users->slice(0,2));

        $this->assertEquals(3, $team->count());
    }

    /** @test */
    public function it_can_remove_all_members_at_once()
    {
    	$team = factory(Team::class)->create();

        $users = factory(User::class, 5)->create();

        $team->add($users);

        $team->removeAll($users);

        $this->assertEquals(0, $team->count());
    }
}
