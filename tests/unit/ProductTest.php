<?php

use App\Product;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ProductTest extends TestCase
{
    protected $product;

    public function setUp()
    {
    	$this->product = new Product('Fallout 4', 40);
    }

    public function testAProductHasName()
    {
        $this->assertEquals('Fallout 4', $this->product->name());
    }

    /** @test */
    public function it_has_cost()
    {
        $this->assertEquals(40, $this->product->cost());
    }
}
