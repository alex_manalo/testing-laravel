<?php

use App\Order;
use App\Product;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class OrderTest extends TestCase
{
    /** @test */
    public function it_consists_of_products()
    {
        $order = new Order();

        $product1 = new Product('Fallout 4', 40);
        $product2 = new Product('Pillowcase', 50);

        $order->add($product1);
        $order->add($product2);

        $this->assertCount(2, $order->products());
    }

    /** @test */
    function it_can_determine_the_total_cost_of_all_its_products()
    {
    	$order = new Order();

        $product1 = new Product('Fallout 4', 40);
        $product2 = new Product('Pillowcase', 50);

        $order->add($product1);
        $order->add($product2);

        $this->assertEquals(90, $order->total());
    }
}
