<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {   
        //1. Visit home page
        $this->visit('/');

        //2. Press "click me" link
        $this->click('click me');

        //3. You should see "You've been clicked punk"
        $this->see("You've been clicked punk");

        //4. Current url should be /feedback
        $this->seePageIs('/feedback');        
    }
}
