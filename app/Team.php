<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{	
	protected $fillable = ['name', 'size'];	

	public function members()
	{
		return $this->hasMany(User::class);
	}

	public function add($users)
	{
		$this->guardAgainstTooManyMembers($users);

		if($users instanceof User)
		{
			return $this->members()->save($users);	
		}

		return $this->members()->saveMany($users);
		
	}

	public function remove($users = null)
	{	
		if($users instanceof User)
		{
			return $users->leaveTeam();
		}		
		
		$this->removeMany($users);
	}

	public function removeMany($users)
	{
		return $this->members()
					 ->whereIn('id', $users->pluck('id'))
					 ->update(['team_id' => null]);
	}

	public function removeAll($users)
	{
		foreach($users as $user)
		{
			$user->leaveTeam();
		}
	}

	public function count()
	{
		return $this->members()->count();
	}

	protected function guardAgainstTooManyMembers($users)
	{
		$numOfUsersToAdd = ($users instanceof User) ? 1 : $users->count();

		$newTeamCount =  $this->count() + $numOfUsersToAdd;

		if($newTeamCount > $this->size)
		{
			throw new \Exception;
		}
	}
}
