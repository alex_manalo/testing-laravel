<?php

namespace App;

use Auth;

trait Likeability {

	public function likes()
	{
		return $this->morphMany(Like::class, 'likeable');
	}

	public function getLikesCountAttribute()
	{
		return $this->likes()->count();
	}

    public function like()
    {
    	$like = new Like([ 'user_id' => Auth::id() ]);

    	$this->likes()->save($like);
    }

    public function isLiked()
    {	
    	return !! $this->likes()
    				   ->where('user_id', Auth::id())
    				   ->count();
    }

    public function unLike()
    {
    	$this->likes()->where('user_id', Auth::id())->delete();
    }

    public function toggle()
    {
    	if($this->isLiked())
    	{
    		return $this->unLike();
    	}

    	$this->like();
    }

}